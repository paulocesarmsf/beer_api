from flask import Flask

from beer_api.blueprints.distributor_blueprint import distributor_blueprint
from beer_api.blueprints.brewerie_blueprint import brewerie_blueprint
from beer_api.blueprints.beer_blueprint import beer_blueprint
from mongo_database import configure_mongo

app = Flask(__name__)
app.register_blueprint(brewerie_blueprint, url_prefix='/brewerie')
app.register_blueprint(beer_blueprint, url_prefix='/beer')
app.register_blueprint(distributor_blueprint, url_prefix='/distributor')


@app.route('/', methods=['GET'])
def index():
    return 'ok'


if __name__ == '__main__':
    configure_mongo()
    app.run(host='0.0.0.0', port=5000, threaded=True)


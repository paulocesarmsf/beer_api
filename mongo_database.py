from mongoengine import *

# Criar arquivo settings, deixar dinamico
MONGO_URI = 'mongodb://mongodb:27017'


class MongoDatabase:

    class __MongoInstance:
        def __init__(self, mongo_uri):
            connect('beer_api', host=mongo_uri, maxpoolsize=10)

    started = False

    def __init__(self, mongo_uri=MONGO_URI):
        if not MongoDatabase.started:
            MongoDatabase.__MongoInstance(mongo_uri)
            MongoDatabase.started = True


class InvalidSateChangeError(Exception):

    def __init__(self, message):
        super(InvalidSateChangeError, self).__init__(message)


def configure_mongo(mongo_url=MONGO_URI):
    MongoDatabase(mongo_url)

import uuid
from mongoengine import Document, StringField, IntField, FloatField, signals
from beer_api.models.brewerie import Brewerie
import logging


class Beer(Document):
    beer_id = StringField(default=lambda: uuid.uuid4().hex, max_length=200, required=True)
    name = StringField(max_length=200, required=True)
    description = StringField(max_length=1000, required=True)
    awards = StringField(max_length=1000, default=None)
    brewerie = StringField(max_length=200, required=True)
    style = StringField(max_length=200, required=True)
    ibu = IntField(required=True)
    abv = FloatField(required=True)
    evaluation = IntField(default=0)
    harmonization = StringField()
    cup = StringField()
    img = StringField()

    @classmethod
    def pre_save(cls, sender, document):
        b = Brewerie.objects(brewerie_id=document.brewerie).first()
        if not b:
            logging.error("Brewerie not found when registering beer: "
                          "{beer}".format(beer=document.name))
            raise Exception
        b.beers.append(document.beer_id)
        b.update(brewerie_id=b.brewerie_id,
                 beers=b.beers)


signals.pre_save.connect(Beer.pre_save, sender=Beer)

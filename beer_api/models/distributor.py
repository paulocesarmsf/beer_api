import uuid
from mongoengine import Document, StringField, BooleanField, DynamicField


class Distributor(Document):
    distributor_id = StringField(default=lambda: uuid.uuid4().hex, max_length=200, required=True)
    name = StringField(max_length=200, required=True)
    address = StringField(max_length=200, required=True)
    phone = StringField(max_length=200, required=True)
    email = StringField(max_length=200, required=True)
    description = StringField(max_length=1000, required=True)
    service_areas = DynamicField(default=[])
    breweries = DynamicField(default=[])
    beers = DynamicField(default=[])
    liters = DynamicField(default=[])
    site = StringField(max_length=250)
    img = StringField()
    daily_delivery = BooleanField(default=False)

import uuid
from mongoengine import Document, StringField, IntField, DynamicField


class Brewerie(Document):
    brewerie_id = StringField(default=lambda: uuid.uuid4().hex, max_length=200, required=True)
    name = StringField(max_length=200, required=True)
    description = StringField(max_length=1000, required=True)
    address = StringField(max_length=200, required=True)
    phone = StringField(max_length=200, required=True)
    evaluation = IntField(default=0)
    beers = DynamicField(default=[])
    img = StringField()


import json

from flask import Blueprint, jsonify, request
from beer_api.models.distributor import Distributor

distributor_blueprint = Blueprint('distributor_blueprint', __name__)


@distributor_blueprint.route('/spec_distributor', methods=['GET'])
def index():
    return 'distributor_blueprint ok!'


@distributor_blueprint.route('/save', methods=['POST'])
def save_distributor():
    try:
        incoming_json = request.get_json()
        distributor = Distributor(name=incoming_json['name'],
                                  address=incoming_json['address'],
                                  phone=incoming_json['phone'],
                                  email=incoming_json['email'],
                                  description=incoming_json['description'],
                                  service_areas=incoming_json['service_areas'],
                                  breweries=incoming_json['breweries'],
                                  beers=incoming_json['beers'],
                                  liters=incoming_json['liters'],
                                  site=incoming_json['site'],
                                  img=incoming_json['img'],
                                  daily_delivery=incoming_json['daily_delivery'])
        distributor.save()
        return jsonify({'success':True, 'id': distributor.distributor_id})
    except Exception as e:
        print(e)
        return jsonify({'success': False})


@distributor_blueprint.route('/update/<id>', methods=['PUT'])
def update_distributor(id):
    try:
        incoming_json = request.get_json()
        distributor = Distributor.objects(distributor_id=id)
        if distributor:
            distributor.update(distributor_id=incoming_json['distributor_id'],
                               name=incoming_json['name'],
                               address=incoming_json['address'],
                               phone=incoming_json['phone'],
                               email=incoming_json['email'],
                               description=incoming_json['description'],
                               service_areas=incoming_json['service_areas'],
                               breweries=incoming_json['breweries'],
                               beers=incoming_json['beers'],
                               liters=incoming_json['liters'],
                               site=incoming_json['site'],
                               img=incoming_json['img'],
                               daily_delivery=incoming_json['daily_delivery'])

            return jsonify({'success':True, 'id': incoming_json['distributor_id']})
        return jsonify({'success': True, 'id': 'Distributor not found'})
    except Exception:
        return jsonify({'success': False})


@distributor_blueprint.route('/find/<id>', methods=['GET'])
def find_distributor(id):
    try:
        distributor = Distributor.objects(distributor_id=id)
        if distributor:
            return jsonify({'success': True, 'data': json.loads(distributor.to_json())})
        return jsonify({'success': True, 'message': "Distributor not found"})
    except Exception:
        return jsonify({'success': False})


@distributor_blueprint.route('/find/all', methods=['GET'])
def find_all_distributor():
    try:
        distributor = Distributor.objects()
        if distributor:
            return jsonify({'success': True, 'data': json.loads(distributor.to_json())})
    except Exception:
        return jsonify({'success': False})


@distributor_blueprint.route('/delete/<id>', methods=['DELETE'])
def delete_distributor(id):
    try:
        distributor = Distributor.objects(distributor_id=id)
        if distributor:
            distributor.delete()
            return jsonify({'success': True, 'data': id})
        return jsonify({'success': True, 'message': "Distributor not found"})
    except Exception:
        return jsonify({'success': False})

import json

from flask import Blueprint, jsonify, request

from beer_api.models.beer import Beer

beer_blueprint = Blueprint('beer_blueprint', __name__)


@beer_blueprint.route('/spec_beer', methods=['GET'])
def index():
    return 'beer_blueprint ok!'


@beer_blueprint.route('/save', methods=['POST'])
def save_beer():
    try:
        incoming_json = request.get_json()
        beer = Beer(name=incoming_json['name'],
                    brewerie=incoming_json['brewerie'],
                    description=incoming_json['description'],
                    awards=incoming_json['awards'],
                    style=incoming_json['style'],
                    ibu=int(incoming_json['ibu']),
                    abv=float(incoming_json['abv']),
                    evaluation=int(incoming_json['evaluation']),
                    img=incoming_json['img'],
                    harmonization=incoming_json['harmonization'],
                    cup=incoming_json['cup'])
        beer.save()
        return jsonify({'success':True, 'id': beer.beer_id})
    except Exception as e:
        print(e)
        return jsonify({'success': False})


@beer_blueprint.route('/update/<id>', methods=['PUT'])
def update_beer(id):
    try:
        incoming_json = request.get_json()
        beer = Beer.objects(beer_id=id)
        if beer:
            beer.update(beer_id=incoming_json['beer_id'],
                        name=incoming_json['name'],
                        brewerie=incoming_json['brewerie'],
                        description=incoming_json['description'],
                        awards=incoming_json['awards'],
                        style=incoming_json['style'],
                        ibu=int(incoming_json['ibu']),
                        abv=float(incoming_json['abv']),
                        evaluation=int(incoming_json['evaluation']),
                        img=incoming_json['img'],
                        harmonization=incoming_json['harmonization'],
                        cup=incoming_json['cup'])
            return jsonify({'success':True, 'id': incoming_json['beer_id']})
        return jsonify({'success': True, 'message': "Beer not found"})
    except Exception:
        return jsonify({'success': False})


@beer_blueprint.route('/find/<id>', methods=['GET'])
def find_beer(id):
    try:
        beer = Beer.objects(beer_id=id)
        if beer:
            return jsonify({'success': True, 'data': json.loads(beer.to_json())})
        return jsonify({'success': True, 'message': "Beer not found"})
    except Exception:
        return jsonify({'success': False})


@beer_blueprint.route('/find/all', methods=['GET'])
def find_all_beers():
    try:
        beer = Beer.objects()
        if beer:
            return jsonify({'success': True, 'data': json.loads(beer.to_json())})
    except Exception:
        return jsonify({'success': False})


@beer_blueprint.route('/delete/<id>', methods=['DELETE'])
def delete_beer(id):
    try:
        beer = Beer.objects(beer_id=id)
        if beer:
            beer.delete()
            return jsonify({'success': True, 'data': id})
        return jsonify({'success': True, 'message': "Beer not found"})
    except Exception:
        return jsonify({'success': False})

import json

from flask import Blueprint, jsonify, request
from beer_api.models.brewerie import Brewerie

brewerie_blueprint = Blueprint('brewerie_blueprint', __name__)


@brewerie_blueprint.route('/spec_brewerie', methods=['GET'])
def index():
    return 'brewerie_blueprint ok!'


@brewerie_blueprint.route('/save', methods=['POST'])
def save_brewerie():
    try:
        incoming_json = request.get_json()
        brewerie = Brewerie(name=incoming_json['name'],
                            description=incoming_json['description'],
                            address=incoming_json['address'],
                            phone=incoming_json['phone'],
                            evaluation=int(incoming_json['evaluation']),
                            img=incoming_json['img'],
                            beers=incoming_json['beers'])
        brewerie.save()
        return jsonify({'success':True, 'id': brewerie.brewerie_id})
    except Exception as e:
        print(e)
        return jsonify({'success': False})


@brewerie_blueprint.route('/update/<id>', methods=['PUT'])
def update_brewerie(id):
    try:
        incoming_json = request.get_json()
        brewerie = Brewerie.objects(brewerie_id=id)
        if brewerie:
            brewerie.update(brewerie_id=incoming_json['brewerie_id'],
                            name=incoming_json['name'],
                            description=incoming_json['description'],
                            address=incoming_json['address'],
                            phone=incoming_json['phone'],
                            evaluation=int(incoming_json['evaluation']),
                            img=incoming_json['img'],
                            beers=incoming_json['beers'])

            return jsonify({'success':True, 'id': incoming_json['brewerie_id']})
        return jsonify({'success': True, 'id': 'Brewerie not found'})
    except Exception:
        return jsonify({'success': False})


@brewerie_blueprint.route('/find/<id>', methods=['GET'])
def find_brewerie(id):
    try:
        brewerie = Brewerie.objects(brewerie_id=id)
        if brewerie:
            return jsonify({'success': True, 'data': json.loads(brewerie.to_json())})
        return jsonify({'success': True, 'message': "Brewerie not found"})
    except Exception:
        return jsonify({'success': False})


@brewerie_blueprint.route('/find/all', methods=['GET'])
def find_all_brewerie():
    try:
        brewerie = Brewerie.objects()
        if brewerie:
            return jsonify({'success': True, 'data': json.loads(brewerie.to_json())})
    except Exception:
        return jsonify({'success': False})


@brewerie_blueprint.route('/delete/<id>', methods=['DELETE'])
def delete_brewerie(id):
    try:
        brewerie = Brewerie.objects(brewerie_id=id)
        if brewerie:
            brewerie.delete()
            return jsonify({'success': True, 'data': id})
        return jsonify({'success': True, 'message': "Brewerie not found"})
    except Exception:
        return jsonify({'success': False})
